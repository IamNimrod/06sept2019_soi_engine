/*------------------------------------------------------------------------\
 |  constants.c : Program Constants                    www.middle-earth.us |
 |  Copyright (C) 2004, Shadows of Isildur: Traithe                        |
 |  Derived under license from DIKU GAMMA (0.0).                           |
 \------------------------------------------------------------------------*/
// Modifications to branch Sectors_Update -Testing.
#include "structs.h"



const char *verbal_number[] = {
  "zero",
  "one",
  "two",
  "three",
  "four",
  "five",
  "six",
  "seven",
  "eight",
  "nine",
  "ten",
  "eleven",
  "twelve",
  "thirteen",
  "fourteen",
  "fifteen",
  "sixteen",
  "seventeen",
  "eighteen",
  "nineteen",
  "twenty",
  "twenty one",
  "twenty two",
  "twenty three",
  "twenty four",
  "twenty five",
  "twenty six",
  "twenty seven",
  "twenty eight",
  "twenty nine",
  "thirty",
  "thirty one",
  "thirty two",
  "thirty three",
  "thirty four",
  "thirty five",
  "thirty six",
  "thirty seven",
  "thirty eight",
  "thirty nine",
  "forty",
  "forty one",
  "forty two",
  "forty three",
  "forty four",
  "forty five",
  "forty six",
  "forty seven",
  "forty eight",
  "forty nine",
  "fifty",
  "fifty one",
  "fifty two",
  "fifty three",
  "fifty four",
  "fifty five",
  "fifty six",
  "fifty seven",
  "fifty eight",
  "fifty nine",
  "sixty",
  "sixty one",
  "sixty two",
  "sixty three",
  "sixty four",
  "sixty five",
  "sixty six",
  "sixty seven",
  "sixty eight",
  "sixty nine",
  "seventy",
  "seventy one",
  "seventy two",
  "seventy three",
  "seventy four",
  "seventy five",
  "seventy six",
  "seventy seven",
  "seventy eight",
  "seventy nine",
  "eighty",
  "eighty one",
  "eighty two",
  "eighty three",
  "eighty four",
  "eighty five",
  "eighty six",
  "eighty seven",
  "eighty eight",
  "eighty nine",
  "ninety",
  "ninety one",
  "ninety two",
  "ninety three",
  "ninety four",
  "ninety five",
  "ninety six",
  "ninety seven",
  "ninety eight",
  "ninety nine",
  "one hundred",
  "\n"
};


const char *verbose_dirs[] = {
  "the north",
  "the east",
  "the south",
  "the west",
  "above",
  "below",
  "outside",
  "inside",
  "the northeast",
  "the northwest",
  "the southeast",
  "the southwest",
  "the upper north",
  "the upper east",
  "the upper south",
  "the upper west",
  "the upper northeast",
  "the upper northwest",
  "the upper southeast",
  "the upper southwest",
  "the lower north",
  "the lower east",
  "the lower south",
  "the lower west",
  "the lower northeast",
  "the lower northwest",
  "the lower southeast",
  "the lower southwest"
  "\n"
};

const char *season_string[] = {
  "spring",
  "summer",
  "autumn",
  "winter",
  "\n"
};

const char *that_time_of_day[] = {
  "day",
  "night",
  "\n"
  };

char *month_short_name[12][21] = {
  "January",
  "February",
  "March",
  "April",
  "May",
  "June",
  "July",
  "August",
  "September",
  "October",
  "November",
  "December"
};


char month_name[12][41] =
{
    "January",
    "February",
    "March",
    "April",
    "May",
    "June",
    "July",
    "August",
    "September",
    "October",
    "November",
    "December"
};


char weekday[7][21] = {
		"Sunday",
		"Monday",
		"Tuesday",
		"Wednesday",
		"Thursday",
		"Friday",
		"Saturday"
	};

char *sector_types[100][21] = {
	"Inside", "Outside", "Road", "Woods", "Mountain", "Swamp", "Cave", "Lake", "River", "Underwater", 
    "\n", "\n", "\n", "\n", "\n", "\n", "\n", "\n", "\n", "\n", 
    "\n", "\n", "\n", "\n", "\n", "\n", "\n", "\n", "\n", "\n", 
    "\n", "\n", "\n", "\n", "\n", "\n", "\n", "\n", "\n", "\n", 
  	"\n", "\n", "\n", "\n", "\n", "\n", "\n", "\n", "\n", "\n", 
	"\n", "\n", "\n", "\n", "\n", "\n", "\n", "\n", "\n", "\n", 
    "\n", "\n", "\n", "\n", "\n", "\n", "\n", "\n", "\n", "\n", 
   	"\n", "\n", "\n", "\n", "\n", "\n", "\n", "\n", "\n", "\n", 
   	"\n", "\n", "\n", "\n", "\n", "\n", "\n", "\n", "\n", "\n", 
   	"\n", "\n", "\n", "\n", "\n", "\n", "\n", "\n", "\n", "\n"
};

char *real_sector_names[100][51] = {
	"inside", "outside", "on a road", "in the woods", "on a mountain", "in a swamp", "in a cave", "in a lake", "in a river", "underwater", 
    "\n", "\n", "\n", "\n", "\n", "\n", "\n", "\n", "\n", "\n", 
    "\n", "\n", "\n", "\n", "\n", "\n", "\n", "\n", "\n", "\n", 
    "\n", "\n", "\n", "\n", "\n", "\n", "\n", "\n", "\n", "\n", 
  	"\n", "\n", "\n", "\n", "\n", "\n", "\n", "\n", "\n", "\n", 
	"\n", "\n", "\n", "\n", "\n", "\n", "\n", "\n", "\n", "\n", 
    "\n", "\n", "\n", "\n", "\n", "\n", "\n", "\n", "\n", "\n", 
   	"\n", "\n", "\n", "\n", "\n", "\n", "\n", "\n", "\n", "\n", 
   	"\n", "\n", "\n", "\n", "\n", "\n", "\n", "\n", "\n", "\n", 
   	"\n", "\n", "\n", "\n", "\n", "\n", "\n", "\n", "\n", "\n"
};

int sector_data[100][11] = {
	{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}, 	{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}, {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
	{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}, 	{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}, {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
	{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}, 	{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}, {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
	{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}, 	{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}, {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
	{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}, 	{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}, {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
	{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}, 	{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}, {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
	{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}, 	{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}, {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
	{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}, 	{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}, {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
	{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}, 	{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}, {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
	{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}, 	{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}, {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
	{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}, 	{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}, {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
	{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}, 	{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}, {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
	{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}, 	{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}, {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
	{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}, 	{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}, {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
	{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}, 	{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}, {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
	{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}, 	{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}, {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
	{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}, 	{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}, {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
	{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}, 	{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}, {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
	{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}, 	{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}, {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
	{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}, 	{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}, {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
	{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}, 	{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}, {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
	{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}, 	{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}, {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
	{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}, 	{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}, {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
	{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}, 	{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}, {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
	{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}, 	{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}, {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
	{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}, 	{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}, {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
	{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}, 	{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}, {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
	{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}, 	{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}, {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
	{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}, 	{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}, {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
	{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}, 	{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}, {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
	{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}, 	{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}, {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
	{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}, 	{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}, {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
	{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}, 	{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}, {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
	{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}
};

const char *somatics[] = {
  "an unknown somatic effect",
  "a headache",
  "a minor concussion",
  "a major concussion",
  "a broken left arm",
  "a broken right arm",
  "a broken right leg",
  "a broken rib",
  "a mysterious illness",
  "an admin punishment",
  "a severed right arm",
  "a severed left arm",
  "a severed right leg",
  "a severed leg leg",
  "a severed head",
  "a severed torso",
  "a severed right hand",
  "a severed right leg",
  "a severed head",
  "\n"
};

char *color_skill[150][3] = {
	"#0", "#0", "#0", "#0", "#0", "#0", "#0", "#0", "#0", "#0",
    "#0", "#0", "#0", "#0", "#0", "#0", "#0", "#0", "#0", "#0",
	"#0", "#0", "#0", "#0", "#0", "#0", "#0", "#0", "#0", "#0",	
	"#0", "#0", "#0", "#0", "#0", "#0", "#0", "#0", "#0", "#0",
	"#0", "#0", "#0", "#0", "#0", "#0", "#0", "#0", "#0", "#0",
	"#0", "#0", "#0", "#0", "#0", "#0", "#0", "#0", "#0", "#0",
	"#0", "#0", "#0", "#0", "#0", "#0", "#0", "#0", "#0", "#0",
	"#0", "#0", "#0", "#0", "#0", "#0", "#0", "#0", "#0", "#0",
	"#0", "#0", "#0", "#0", "#0", "#0", "#0", "#0", "#0", "#0",
	"#0", "#0", "#0", "#0", "#0", "#0", "#0", "#0", "#0", "#0",
	"#0", "#0", "#0", "#0", "#0", "#0", "#0", "#0", "#0", "#0",
	"#0", "#0", "#0", "#0", "#0", "#0", "#0", "#0", "#0", "#0",
	"#0", "#0", "#0", "#0", "#0", "#0", "#0", "#0", "#0", "#0",
	"#0", "#0", "#0", "#0", "#0", "#0", "#0", "#0", "#0", "#0",
	"#0", "#0", "#0", "#0", "#0", "#0", "#0", "#0", "#0", "#0"
};

int restricted_skills[150] = {
-2, -2, -2, -2, -2, -2, -2, -2, -2, -2,
-2, -2, -2, -2, -2, -2, -2, -2, -2, -2,
-2, -2, -2, -2, -2, -2, -2, -2, -2, -2,
-2, -2, -2, -2, -2, -2, -2, -2, -2, -2,
-2, -2, -2, -2, -2, -2, -2, -2, -2, -2,
-2, -2, -2, -2, -2, -2, -2, -2, -2, -2,
-2, -2, -2, -2, -2, -2, -2, -2, -2, -2,
-2, -2, -2, -2, -2, -2, -2, -2, -2, -2,
-2, -2, -2, -2, -2, -2, -2, -2, -2, -2,
-2, -2, -2, -2, -2, -2, -2, -2, -2, -2,
-2, -2, -2, -2, -2, -2, -2, -2, -2, -2,
-2, -2, -2, -2, -2, -2, -2, -2, -2, -2,
-2, -2, -2, -2, -2, -2, -2, -2, -2, -2,
-2, -2, -2, -2, -2, -2, -2, -2, -2, -2,
-2, -2, -2, -2, -2, -2, -2, -2, -2, -2
};

// char *skills[150][40] = {
char *skills[150][40] = {	
	"Unused",
	"\n", "\n", "\n", "\n", "\n", "\n", "\n", "\n", "\n", "\n",
	"\n", "\n", "\n", "\n", "\n", "\n", "\n", "\n", "\n", "\n",
	"\n", "\n", "\n", "\n", "\n", "\n", "\n", "\n", "\n", "\n",
	"\n", "\n", "\n", "\n", "\n", "\n", "\n", "\n", "\n", "\n",
	"\n", "\n", "\n", "\n", "\n", "\n", "\n", "\n", "\n", "\n",
	"\n", "\n", "\n", "\n", "\n", "\n", "\n", "\n", "\n", "\n",
	"\n", "\n", "\n", "\n", "\n", "\n", "\n", "\n", "\n", "\n",
	"\n", "\n", "\n", "\n", "\n", "\n", "\n", "\n", "\n", "\n",
	"\n", "\n", "\n", "\n", "\n", "\n", "\n", "\n", "\n", "\n",
	"\n", "\n", "\n", "\n", "\n", "\n", "\n", "\n", "\n", "\n",
	"\n", "\n", "\n", "\n", "\n", "\n", "\n", "\n", "\n", "\n",
	"\n", "\n", "\n", "\n", "\n", "\n", "\n", "\n", "\n", "\n",
	"\n", "\n", "\n", "\n", "\n", "\n", "\n", "\n", "\n", "\n",
	"\n", "\n", "\n", "\n", "\n", "\n", "\n", "\n", "\n", "\n",
	"\n", "\n", "\n", "\n", "\n", "\n", "\n", "\n", "\n", "\n"
  };
  
char mud_name[41] = "Default mud name."; // Set this value in registry text file.
char msgs_name[41] = "Hobbitmail"; // Set this value in registry text file.
int debug_build = 0; 
char who_data[51] = "people online."; // Set this value in registry text file.
char encrypt_pass[41] = "change in registry"; // Set this value in registry text file.
char staff_email[41] = "replacethisinregistry@max40.com"; // Set this value in registry text file.
int last_skill = 0; 
int public_pfile = 0; // Set this value in registry text file.
int census_high_cut_off = 60; // set this value in registry text file.
int census_med_cut_off = 40; // set this value in registry text file.
int census_low_cut_off = 20; // set this value in registry text file.
int census_skills_max = 27;  // set this value in registry text file.
int census_skills_list[100];
std::string SKILL_NOUNS[] = {"Hunters", "Practitioners of Medicine", "Foragers", "Weaponmakers", "Armormakers", "Metalwrights", "Leatherworkers", "Textilecrafters", "Woodworkers", "Cooks", "Bakers", "Brewers", "Fishers", "Stonecrafters", "Earthcrafters", "Gardeners", "Farmers", "Dyecrafters", "Boatmakers", "Bonecrafters", "Gem crafters", "Distillers", "Miners", "Herbalists", "Waxcrafters", "Winecrafters", "Jewelrycraft"}; 
std::string high_cut_off_s = "Master"; // set this value in registry text file.
std::string med_cut_off_s = "Journeyman"; // set this value in registry text file.
std::string low_cut_off_s = "Apprentice"; // set this value in registry text file.
int window_low = 75; //set this value in the registry text file
int window_high = 75; //set this value in the registry text file
char app_email[41] = "replacethisinregistry@max40.com"; // Set this value in registry text file.
char code_email[41] = "replacethisinregistry@max40.com"; // Set this value in registry text file.
char pet_email[41] = "replacethisinregistry@max40.com"; // Set this value in registry text file.
char report_email[41] = "replacethisinregistry@max40.com"; // Set this value in registry text file.
char mud_email[41] = "replacethisinregistry@max40.com"; // Set this value in registry text file.
char implementor_account[16] = "impdefault"; // Set this value in registry text file.
char path_to_sendmail[41] = "/usr/sbin/sendmail"; // Set this value in registry text file.
char path_to_website[41] = "/usr/share/doxy"; // Set this value in registry text file.
char help_append[141] = "Update help_append in registry."; // Set this value in registry text file.
char game_url[41] = "gameaddress.com"; // Set this value in registry text file.
char the_sun[21] = "The Sun"; // Set this value in registry text file.
char the_moon[21] = "The Moon"; // Set this value in the registry text file.
float constitution_multiplier = 4; // Set this value in registry text file.
float combat_brutality = 1.00; // Set this value in registry text file.
int area_one_low = 0; // Set this value in registry text file.
int area_one_high = 0; // Set this value in registry text file.
int area_two_low = 0; // Set this value in registry text file.
int area_two_high = 0; // Set this value in registry text file.
int area_three_low = 0;  // Set this value in registry text file.
int area_three_high = 0; // Set this value in registry text file.
int area_four_low = 0; // Set this value in registry text file.
int area_four_high = 0; // Set this value in registry text file.
int area_five_low = 0; // Set this value in registry text file.
int area_five_high = 0; // Set this value in registry text file.
char area_one_single[51] = "area_one_single set in the registry file."; // Set this value in registry text file.
char area_one_multi[51] = "area_one_multi set in the registry file."; // Set this value in registry text file.
char area_two_single[51] = "area_two_single set in the registry file."; // Set this value in registry text file.
char area_two_multi[51] = "area_two_multi set in the registry file."; // Set this value in registry text file.
char area_three_single[51] = "area_three_single set in the registry file."; // Set this value in registry text file.
char area_three_multi[51] = "area_three_multi set in the registry file."; // Set this value in registry text file.
char area_four_single[51] = "area_four_single set in the registry file."; // Set this value in registry text file.
char area_four_multi[51] = "area_four_multi set in the registry file."; // Set this value in registry text file.
char area_five_single[51] = "area_five_single set in the registry file."; // Set this value in registry text file.
char area_five_multi[51] = "area_five_multi set in the registry file."; // Set this value in registry text file.

const char *preg_msgs_dflt[] = {
"You feel nauseous.", 
"You feel nauseous, the rise of bile burning the back of your throat.", 
"You feel like emptying everything in your stomach.", 
"You feel the onset of gagging.", 
"You are about to vomit.", 
"You feel bloated.", 
"You feel a soft, dull headache pulse within your temples.", 
"You feel suddenly quite melancholy.", 
"You feel irritable.", 
"You feel giddy of a sudden.", 
"You feel antsy and on edge.", 
"You feel like crying.", 
"You feel exhausted.", 
"You feel every scent heightened for good or for bad.", 
"Your breasts feel tender.", 
"You feel the urge to urinate.", 
"You feel an urgent need to urinate.", 
"You feel you must see to a latrine, immediately!",
"You feel overheated.", 
"You feel dizzy.", 
"You feel lightheaded.", 
"Your thoughts feel fuzzy.", 
"You feel uncomfortable.", 
"You feel your clothing seems tighter and uncomfortable.", 
"You feel bloated and uncomfortable, as though constipated.", 
"You feel like you are forgetting something.", 
"You feel like you are gaining weight.", 
"You feel uncomfortable with the changes in your body, feeling unattractive.", 
"You cannot mentally focus right now.", 
"You feel your mind wander and you drift, forgetting whatever it is you are doing at the moment.", 
"Did you forget something? You are uncertain and now you're worried.", 
"You feel so very tired but also uncomfortable as if you're sure, if you slept you would not be able to settle.", 
"You feel exhausted but unable to sleep.", 
"You are very hungry of a sudden.", 
"You cannot seem to feel sated.", 
"You are craving something extremely salty or pickled.", 
"You are craving fried chips.", 
"You are craving peaches.", 
"You are craving strawberries.", 
"You are craving plums.", 
"You are craving cheese.", 
"You are craving greasy, roast meat.", 
"You are craving kidney pie.", 
"You are craving something very sweet.", 
"You are craving fish.", 
"You are craving eggs.", 
"You are craving butter.", 
"You are craving fresh, still warm milk.", 
"You are craving pickled, boiled cabbage.", 
"You are craving onions.", 
"You are craving beer.", 
"The smell of dirt makes you salivate.", 
"The smell of cooked meat makes you extremely nauseous.", 
"Strong, sweet smells like soap or perfume make you want to flee, immediately nauseous.", 
"The smell of fish makes you extremely nauseous and uncomfortable.", 
"The smell of mead makes you immediately nauseous.",  
"The smell of salad greens make you immediately and extremely nauseous.", 
"The idea of eating carrots repels you, making you nauseous.", 
"The idea of drinking milk repels you, making you nauseous.", 
"Your left calf cramps up.", 
"Your right calf cramps up.", 
"Your left thigh cramps up.", 
"Your right thigh cramps up.", 
"Your left ankle is swollen.", 
"Your right ankle is swollen.", 
"You feel a hiccup of heartburn.", 
"You have indigestion.", 
"You feel a burst of energy.",
"\n"
};

const char *preg_msgs_zero[] = {
"You feel hungry.", 
"You feel tired.",
"\n"
};

const char *preg_msgs_one[] = {
"You feel a soft, dull ache in the small of your lower back and it lingers, pulsing for a while.", 
"Your lower back aches a little.", 
"You feel a dull, cramping pain in your womb.",
"\n"
};

const char *preg_msgs_two[] = {
"You feel stretched, your hips aches and you feel a dull throb of pain in your abdomen.", 
"Your eyes are dry, itchy, and sensitive to light and color.", 
"Your feel like your hips are widening.",
"\n"
};

const char *preg_msgs_three[] = {
"You feel the soft flutter of quickening within you.", 
"You feel a brush of movement inside, like feathers.", 
"You feel something like bubbles move within your womb.", 
"You feel a faint, strange movement in your womb.",
"\n"
};

const char *preg_msgs_four[] = {
"You feel the soft flutter of movement within you as the baby shifts.", 
"You feel a soft kick.", 
"You feel the baby move.", 
"You feel battered from within as the baby kicks you a few times.",
"\n"
};

const char *preg_msgs_five[] = {
"The baby rolls to one side.", 
"The baby seems to be dancing in you or exploring your insides.", 
"It feels like the baby has hiccups.", 
"You feel the faint flutter of movement as the baby wakens and stretches within you.", 
"You feel the soft drum of a heartbeat if you put a palm to your belly.", 
"You feel the faint flutter as baby turn over within you.", 
"You feel the baby kick you softly from inside.",
"\n"
};

const char *preg_msgs_six[] = {
"You feel your clothing cannot possibly fit you, you feel far too heavy, bloated, and uncomfortable.", 
"You have gained weight, you feel so big and bloated.", 
"Your feel your face flushed and itchy, the onset of a rash that lingers for days.", 
"You feel so heavy.", 
"You feel like you're going to topple over.", 
"You feel ready to pop.", 
"You feel so very tired, tired of being bloated, tired of being so heavy, tired of being so tired, tired of not sleeping, tired of everything, and now you're tired of tears because you start to cry.", 
"You feel like you want to meet your baby already.", 
"The baby rolls over within you.", 
"The baby gives your ribs a quick kick.", 
"The baby has hiccups inside of you, and you can feel each one.", 
"You feel the baby waken and stretch within you.", 
"You feel the soft drum of a heartbeat if you put a palm to your belly.", 
"You feel the baby turn over within you.", 
"You feel the baby kick you roughly from inside.",
"\n"
};

const char *post_preg_msgs_dflt[] = {
"Your breasts are swollen and painful, like you should nurse.", 
"Your breasts are tender.", 
"Your body aches from your recent birthing.", 
"You feel like you should nurse..", 
"You feel voraciously hungry.", 
"You feel your thirst is hard to quench..", 
"You feel extremely moody.", 
"You feel easily irritated.", 
"You feel on the brink of tears.",
"\n"
};

const char *post_preg_msgs_zero[] = {
"Your abdomen feels like you're still pregnant, round and soft.", 
"You still feel heavy and bloated from pregnancy.", 
"Your abdomen feels thick and plush post-birthing.",
"\n"
};

const char *post_preg_msgs_one[] = {
"Your abdomen feels like it is getting smaller, recently still thick with recent birthing.", 
"You feel chapped from nursing.", 
"You feel exhausted.",
"\n"
};

const char *post_preg_msgs_two[] = {
"You feel your body is slowly getting back to normal though your breasts remain thick with milk.", 
"You feel a little sensitive, emotionally.", 
"You feel like your abdomen is getting firmer, less soft with your recent pregnancy.",
"\n"
};

const char *where[] = {
  "<used as light>          ",	// 0
  "<worn on finger>         ",	// 1
  "<worn on finger>         ",
  "<worn at neck>           ",
  "<worn at neck>           ",
  "<worn on body>           ",
  "<worn on head>           ",
  "<worn on legs>           ",
  "<worn on feet>           ",
  "<worn on hands>          ",
  "<worn on arms>           ",	// 10
  "<worn as shield>         ",
  "<worn about body>        ",
  "<worn about waist>       ",
  "<worn on right wrist>    ",
  "<worn on left wrist>     ",
  "<wielded primary>        ",
  "<wielded secondary>      ",
  "<wielded both hands>     ",
  "<worn around the chest>  ",
  "<worn on belt>           ",	// 20
  "<worn on belt>           ",
  "<across the back>        ",
  "<over the eyes>          ",
  "<worn at throat>         ",
  "<worn on the ears>       ",
  "<worn over shoulder>     ",
  "<worn over shoulder>     ",
  "<worn on right ankle>    ",
  "<worn on left ankle>     ",
  "<worn in hair>           ",	// 30
  "<worn on face>           ",
  "",
  "",
  "<about upper right arm>  ",	// 34
  "<about upper left arm>   ",	// 35
  "<worn over the body>     ",	// 36
  "<worn over the eyes>     ",	// 37
  "<worn around the hips>   ",  // 38
  "<worn on belt>           ",  //40
  "<worn as an error>       ",  //41
  "\n"
};

const char *locations[] = {
  "hand",
  "finger",
  "finger",
  "neck",
  "neck",
  "body",
  "head",
  "legs",
  "feet",
  "hands",
  "arms",
  "hands",
  "body",
  "waist",
  "wrist",
  "wrist",
  "hand",
  "hand",
  "hand",
  "hand",
  "belt",
  "belt",
  "back",
  "eyes",
  "throat",
  "ears",
  "shoulder",
  "shoulder",
  "ankle",
  "ankle",
  "hair",
  "face",
  "something",
  "something",
  "arm",
  "arm",
  "body",
  "eyes",
  "\n"
};


const char *color_liquid[] = {
  "clear",
  "brown",
  "clear",
  "brown",
  "dark",
  "golden",
  "red",
  "green",
  "clear",
  "light green",
  "white",
  "brown",
  "black",
  "red",
  "clear",
  "black"
};

const char *fullness[] = {
  "less than half ",
  "about half ",
  "more than half ",
  ""
};


const char *exit_bits[] = {
  "IsDoor",
  "Closed",
  "Locked",
  "RSClosed",
  "RSLocked",
  "PickProof",
  "Secret",
  "Trapped",
  "Toll",
  "IsGate",
  "\n"
};

extern const int electric_list [] = {
	ITEM_E_RADIO,
	ITEM_E_LIGHT,
	ITEM_E_PHONE,
	ITEM_E_BOOST,
	ITEM_E_REMOTE,
	ITEM_E_BUG,
	ITEM_E_CLOCK,
	ITEM_E_MEDICAL,
	ITEM_E_GOGGLE,
	ITEM_E_BOOK,
	ITEM_E_BREATHER,
};

const char *seasons[] = {
  "Spring",
  "Summer",
  "Autumn",
  "Winter",
  "\n"
};

const char *affected_bits[] = {
  "Undefined",
  "Invisible",
  "Infravision",
  "Detect-Invisible",
  "Detect-Magic",
  "Sense-Life",			/* 5 */
  "Transporting",
  "Sanctuary",
  "Group",
  "Curse",
  "Magic-only",			/* 10 */
  "Poison",
  "AScan",
  "AFallback",
  "Undefined",
  "Undefined",			/* 15 */
  "Sleep",
  "Dodge",
  "ASneak",
  "AHide",
  "Fear",			/* 20 */
  "Follow",
  "Hooded",
  "Charm",			/* was affected_bits[21] */
  "\n"
};

const char *smallgood_types[] = {
  "smallgoods",
  "ore",
  "grain",
  "fur",
  "meat",
  "\n"
};

const char *action_bits[] = {
  "Memory",
  "Sentinel",
  "Rescuer",
  "IsNPC",
  "NoVNPC",
  "Aggressive",
  "Stayzone",
  "Fixer",
  "Sent-Aggro",
  "BulkTrader",
  "Shooter",
  "NoBuy",
  "Enforcer",
  "PackAnimal",
  "Vehicle",
  "Stop",
  "Squeezer",
  "Pariah",
  "Mount",
  "Scented",                   /* Mob has, or should have, some venom */
  "PCOwned",
  "Wildlife",			/* Mob won't attack other wildlife */
  "Stayput",			/* Mob saves and reloads after boot */
  "Passive",			/* Mob won't assist clan brother in combat */
  "Auctioneer",			/* Mob is an auctioneer - auctions.cpp */
  "Econzone",			/* NPC, if keeper, uses econ zone price dis/markups */
  "Jailer",
  "\n"
};


const char *position_types[] = {
  "Dead",
  "Mortally wounded",
  "Unconscious",
  "Stunned",
  "Sleeping",
  "Resting",
  "Sitting",
  "Fighting",
  "Standing",
  "\n"
};

const char *connected_types[] = {
  "Playing",
  "Entering Name",
  "Confirming Name",
  "Entering Password",
  "Entering New Password",
  "Confirming New password",
  "Choosing Gender",
  "Reading Message of the Day",
  "Main Menu",
  "Changing Password",
  "Confirming Changed Password",
  "Rolling Attributes",
  "Selecting Race",
  "Decoy Screen",
  "Creation Menu",
  "Selecting Attributes",
  "New Player Menu",
  "Documents Menu",
  "Selecting Documentation",
  "Reading Documentation",
  "Picking Skills",
  "New Player",
  "Age Select",
  "Height-Frame Select",
  "New Char Intro Msg",
  "New Char Intro Wait",
  "Creation Comment",
  "Read Reject Message",
  "Web Connection",
  "\n"
};

const char *sex_types[] = {
  "Sexless",
  "Male",
  "Female",
  "\n"
};

const char *sex_noun[] = {
  "it",
  "him",
  "her",
  "\n",
};

const char *weather_room[] = {
  "default",
  "foggy",
  "cloudy",
  "rainy",
  "stormy",
  "snowy",
  "blizzard",
   "\n"
};
