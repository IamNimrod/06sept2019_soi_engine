# SoI Engine #

An open-source version of the Shadows of Isildur Game Engine.

* All are welcome to use and contribute to the codebase in this project so long as the license requirements listed below are fully met.

### Documentation ###

We keep Doxygen Documentation here: http://middle-earth.us/doxy/html/index.html 
(note the lack of 'www' in the url) - It is updated daily from the latest source files.

### What is our goal? ###

* To provide a completely non-branded version of the SoI Engine that is available to any who wish to use it.
* To provide a completely reverse-compatible codebase in order to support any games using this engine.


### How do I get set up? ###

* Setup procedure coming soon.


### Contribution guidelines ###

* All contributions must be thoroughly tested prior to submission.
* Contributions must be made in accordance with the open-source license listed at the bottom of this document.
* No further copyright demands will be entertained.
* All contributions become the property of the whole and do not grant rights of any kind to the submitter.


### What is the Shadow Cooperative ###

* The Shadow Cooperative was established to further the work on the SoI Engine codebase and not limit ourselves to any single person.  This will allow the project to continue should any one individual leave the project.

* Currently, all Shadow Cooperative members are administrators of the flag-ship game Shadows of Isildur, which is the parent organization overseeing this open-source initiative.

* Non members are openly welcomed and encouraged to participate.


Licensing is based on the BSD 3 Clause License

Copyright (c) 2015 Shadow Cooperative
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.

2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.

3. Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.